package ru.tsc.mordovina.tm.exception.empty;

import ru.tsc.mordovina.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error. Id is empty");
    }

    public EmptyIdException(String value) {
        super("Error. " + value + " id is empty");
    }

}
