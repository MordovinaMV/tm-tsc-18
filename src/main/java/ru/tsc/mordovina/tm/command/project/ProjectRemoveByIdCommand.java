package ru.tsc.mordovina.tm.command.project;

import ru.tsc.mordovina.tm.command.AbstractProjectCommand;
import ru.tsc.mordovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return "project-remove-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove project by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(id);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectTaskService().removeById(id);
    }

}
