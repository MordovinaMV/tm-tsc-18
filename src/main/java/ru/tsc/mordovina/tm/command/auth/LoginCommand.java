package ru.tsc.mordovina.tm.command.auth;

import ru.tsc.mordovina.tm.command.AbstractAuthCommand;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public class LoginCommand extends AbstractAuthCommand {

    @Override
    public String getCommand() {
        return "login";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Login user to system";
    }

    @Override
    public void execute() {
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

}
