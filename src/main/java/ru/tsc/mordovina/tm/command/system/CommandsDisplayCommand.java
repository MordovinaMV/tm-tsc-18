package ru.tsc.mordovina.tm.command.system;

import ru.tsc.mordovina.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandsDisplayCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return "commands";
    }

    @Override
    public String getArgument() {
        return "-cmd";
    }

    @Override
    public String getDescription() {
        return "Display list of commands";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands)
            System.out.println(command.getCommand());
    }

}
