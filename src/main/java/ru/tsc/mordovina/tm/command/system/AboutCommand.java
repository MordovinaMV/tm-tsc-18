package ru.tsc.mordovina.tm.command.system;

import ru.tsc.mordovina.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Developer info";
    }

    @Override
    public void execute() {
        System.out.println("Developer: Mary Mordovina");
        System.out.println("e-mail: mmordovinamv@gmail.com");
    }

}
