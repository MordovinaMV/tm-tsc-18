package ru.tsc.mordovina.tm.command.task;

import ru.tsc.mordovina.tm.command.AbstractProjectTaskCommand;
import ru.tsc.mordovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.mordovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.model.Task;
import ru.tsc.mordovina.tm.util.TerminalUtil;

import java.util.List;

public class TaskListByProjectIdProjectTaskCommand extends AbstractProjectTaskCommand {

    @Override
    public String getCommand() {
        return "task-list-by-project-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Display list of tasks by project id";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        final List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(projectId);
        if (tasks.size() <= 0) throw new TaskNotFoundException();
        for (Task task : tasks)
            task.toString();
    }

}
