package ru.tsc.mordovina.tm.command.user;

import ru.tsc.mordovina.tm.command.AbstractUserCommand;
import ru.tsc.mordovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.mordovina.tm.exception.system.AccessDeniedException;
import ru.tsc.mordovina.tm.model.User;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public class UserDisplayByIdCommand extends AbstractUserCommand {

    @Override
    public String getCommand() {
        return "user-display-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Display user by id";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAuth || !isAdmin) throw new AccessDeniedException();
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findUserById(id);
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

}
