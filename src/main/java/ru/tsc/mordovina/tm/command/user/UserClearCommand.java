package ru.tsc.mordovina.tm.command.user;

import ru.tsc.mordovina.tm.command.AbstractUserCommand;
import ru.tsc.mordovina.tm.exception.system.AccessDeniedException;

public class UserClearCommand extends AbstractUserCommand {

    @Override
    public String getCommand() {
        return "user-clear";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Clear all users";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAuth || !isAdmin) throw new AccessDeniedException();
        serviceLocator.getUserService().clear();
    }

}
