package ru.tsc.mordovina.tm.command.project;

import ru.tsc.mordovina.tm.command.AbstractProjectCommand;
import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return "project-change-status-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Change project status by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        serviceLocator.getProjectService().changeStatusById(id, status);
    }

}
