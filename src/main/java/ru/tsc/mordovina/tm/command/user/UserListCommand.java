package ru.tsc.mordovina.tm.command.user;

import ru.tsc.mordovina.tm.command.AbstractUserCommand;
import ru.tsc.mordovina.tm.exception.empty.EmptyUserListException;
import ru.tsc.mordovina.tm.exception.system.AccessDeniedException;
import ru.tsc.mordovina.tm.model.User;

import java.util.List;

public class UserListCommand extends AbstractUserCommand {

    @Override
    public String getCommand() {
        return "user-list";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Display list of users";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAuth || !isAdmin) throw new AccessDeniedException();
        System.out.println("User list");
        final List<User> users = serviceLocator.getUserService().findAll();
        if (null == users) throw new EmptyUserListException();
        for (final User user : users)
            showUser(user);
    }

}
