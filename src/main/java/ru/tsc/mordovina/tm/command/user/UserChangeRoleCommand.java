package ru.tsc.mordovina.tm.command.user;

import ru.tsc.mordovina.tm.command.AbstractUserCommand;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.mordovina.tm.exception.system.AccessDeniedException;
import ru.tsc.mordovina.tm.model.User;
import ru.tsc.mordovina.tm.util.TerminalUtil;

import java.util.Arrays;

public class UserChangeRoleCommand extends AbstractUserCommand {
    @Override
    public String getCommand() {
        return "user-change-role";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Changes user role";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAuth || !isAdmin) throw new AccessDeniedException();
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findUserById(id);
        if (user == null) throw new UserNotFoundException();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (currentUserId.equals(user.getId())) throw new AccessDeniedException();
        System.out.println("Enter role:");
        System.out.println(Arrays.toString(Role.values()));
        final String roleValue = TerminalUtil.nextLine();
        final Role role = Role.valueOf(roleValue);
        serviceLocator.getUserService().setRole(id, role);
    }
}
