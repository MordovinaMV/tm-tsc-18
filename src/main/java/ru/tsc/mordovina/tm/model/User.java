package ru.tsc.mordovina.tm.model;

import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.util.HashUtil;

import java.util.UUID;

public class User {

    private String id = UUID.randomUUID().toString();

    private String login;

    private String password;

    private String email;

    private String lastName;

    private String firstName;

    private String middleName;

    private Role role = Role.USER;

    public User(String login, String password) {
        this.login = login;
        this.password = HashUtil.salt(password);
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Role getRole() {
        return role;
    }

    public String getId() {
        return id;
    }

}