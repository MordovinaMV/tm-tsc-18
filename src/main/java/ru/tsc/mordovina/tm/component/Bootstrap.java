package ru.tsc.mordovina.tm.component;

import ru.tsc.mordovina.tm.api.repository.*;
import ru.tsc.mordovina.tm.api.service.*;
import ru.tsc.mordovina.tm.command.AbstractCommand;
import ru.tsc.mordovina.tm.command.auth.LoginCommand;
import ru.tsc.mordovina.tm.command.auth.LogoffCommand;
import ru.tsc.mordovina.tm.command.project.*;
import ru.tsc.mordovina.tm.command.system.*;
import ru.tsc.mordovina.tm.command.task.*;
import ru.tsc.mordovina.tm.command.user.*;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.exception.system.UnknownCommandException;
import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.model.Task;
import ru.tsc.mordovina.tm.model.User;
import ru.tsc.mordovina.tm.repository.*;
import ru.tsc.mordovina.tm.service.*;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILogService logService = new LogService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthRepository authRepository = new AuthRepository();

    private final IAuthService authService = new AuthService(authRepository, userService);


    {
        registry(new CommandsDisplayCommand());
        registry(new AboutCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new ProjectListShowCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListShowCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskListByProjectIdProjectTaskCommand());
        registry(new TaskAddToProjectByIdProjectTaskCommand());
        registry(new TaskRemoveFromProjectByIdProjectTaskCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new LoginCommand());
        registry(new LogoffCommand());
        registry(new UserListCommand());
        registry(new UserCreateCommand());
        registry(new UserClearCommand());
        registry(new UserChangeRoleCommand());
        registry(new UserDisplayByIdCommand());
        registry(new UserDisplayByLoginCommand());
        registry(new UserPasswordChangeCommand());
        registry(new UserRemoveByIdCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserUpdateByIdCommand());
        registry(new UserUpdateByLoginCommand());
    }

    public void start(final String... args) {
        displayWelcome();
        runArgs(args);
        initData();
        initUsers();
        logService.debug("Test environment");
        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                final String command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }

    private void initData() {
        projectService.add(new Project("Project A", "-"));
        projectService.add(new Project("Project D", "-"));
        projectService.add(new Project("Project C", "-"));
        projectService.add(new Project("Project B", "-"));
        taskService.add(new Task("Task C", "-"));
        taskService.add(new Task("Task B", "-"));
        taskService.add(new Task("Task D", "-"));
        taskService.add(new Task("Task A", "-"));
        projectService.finishByName("Project A");
        projectService.startByName("Project C");
        taskService.finishByName("Task C");
        taskService.startByName("Task D");
    }

    public void initUsers() {
        final User user = new User("user", "user");
        user.setEmail("user@email.ru");
        userService.add(user);
        final User admin = new User ("admin", "admin");
        admin.setRole(Role.ADMIN);
        userService.add(admin);
    }


    private void displayWelcome() {
        System.out.println("** Welcome to task manager **");
    }

    private boolean runArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        AbstractCommand command = commandService.getCommandByName(args[0]);
        if (command == null) throw new UnknownCommandException(args[0]);
        command.execute();
        return true;
    }

    private void runCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        abstractCommand.execute();
    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
