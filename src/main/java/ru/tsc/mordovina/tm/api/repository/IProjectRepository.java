package ru.tsc.mordovina.tm.api.repository;

import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    boolean existsById(String id);

    boolean existsByIndex(int index);

    boolean existsByName(String name);

    void add(Project project);

    void remove(Project project);

    void clear();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(int index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(int index);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusByName(String name, Status status);

    Integer getSize();

}
