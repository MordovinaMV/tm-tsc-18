package ru.tsc.mordovina.tm.api.service;

public interface IAuthService {

    String getCurrentUserId();

    void setCurrentUserId(String userId);

    boolean isAuth();

    boolean isAdmin();

    void login(String login, String password);

    void logout();

}
