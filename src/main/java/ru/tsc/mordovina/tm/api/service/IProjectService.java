package ru.tsc.mordovina.tm.api.service;

import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    void clear();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final Integer index, final String name, final String description);

    boolean existsById(String id);

    boolean existsByIndex(int index);

    boolean existsByName(String name);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusByName(String name, Status status);

}
