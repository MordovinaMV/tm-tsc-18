package ru.tsc.mordovina.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}