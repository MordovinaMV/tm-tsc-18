package ru.tsc.mordovina.tm.api.repository;

import ru.tsc.mordovina.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    void add(User user);

    void remove(User user);

    void clear();

    User findUserById(String id);

    User findUserByLogin(String login);

    User findUserByEmail(String email);

    User removeUserById(String id);

    User removeUserByLogin(String login);

    boolean userExistsByLogin(String login);

    boolean userExistsByEmail(String email);

}
