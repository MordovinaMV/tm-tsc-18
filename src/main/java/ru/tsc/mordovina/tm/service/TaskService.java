package ru.tsc.mordovina.tm.service;

import ru.tsc.mordovina.tm.api.repository.ITaskRepository;
import ru.tsc.mordovina.tm.api.service.ITaskService;
import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.exception.empty.*;
import ru.tsc.mordovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.mordovina.tm.exception.system.IndexIncorrectException;
import ru.tsc.mordovina.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        if (comparator == null) return Collections.emptyList();
        return taskRepository.findAll(comparator);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(id);
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(name);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(name);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.existsById(id);
    }

    @Override
    public boolean existsByIndex(final int index) {
        return taskRepository.existsByIndex(index);
    }

    @Override
    public boolean existsByName(final String name) {
        return taskRepository.existsByName(name);
    }

    @Override
    public Task startById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.startById(id);
    }

    @Override
    public Task startByIndex(final Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.startByIndex(index);
    }

    @Override
    public Task startByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.startByName(name);
    }

    @Override
    public Task finishById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.finishById(id);
    }

    @Override
    public Task finishByIndex(final Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        return taskRepository.finishByIndex(index);
    }

    @Override
    public Task finishByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.finishByName(name);
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeStatusById(id, status);
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Task changeStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeStatusByName(name, status);
    }

}