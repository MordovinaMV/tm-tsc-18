package ru.tsc.mordovina.tm.service;

import ru.tsc.mordovina.tm.api.repository.IUserRepository;
import ru.tsc.mordovina.tm.api.service.IUserService;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.exception.empty.*;
import ru.tsc.mordovina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.mordovina.tm.exception.entity.UserEmailExistsException;
import ru.tsc.mordovina.tm.exception.entity.UserLoginExistsException;
import ru.tsc.mordovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.mordovina.tm.model.User;
import ru.tsc.mordovina.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void add(final User user) {
        if (user == null) throw new UserNotFoundException();
        userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExists(login)) throw new UserLoginExistsException(login);
        final User user = new User(login, HashUtil.salt(password));
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExists(email)) throw new UserEmailExistsException(email);
        final User user = create(login, password);
        user.setEmail(email);
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        user.setRole(role);
        userRepository.add(user);
        return user;
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findUserById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPassword(HashUtil.salt(password));
        return user;
    }

    @Override
    public User setRole(final String id, final Role role) {
        if (id == null || id.isEmpty()) throw new EmptyLoginException();
        if (role == null) throw new EmptyRoleException();
        final User user = findUserById(id);
        if (user == null) throw new UserNotFoundException();
        user.setRole(role);
        return user;
    }

    @Override
    public void remove(final User user) {
        userRepository.remove(user);
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public User findUserById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findUserById(id);
    }

    @Override
    public User findUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findUserByLogin(login);
    }

    @Override
    public User findUserByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findUserByEmail(email);
    }

    @Override
    public User removeUserById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeUserById(id);
    }

    @Override
    public User removeUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findUserByLogin(login);
    }

    @Override
    public boolean isLoginExists(final String login) {
        return userRepository.findUserByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(final String email) {
        return userRepository.findUserByEmail(email) != null;
    }

    @Override
    public User updateUserById(final String id, final String lastName, final String firstName,
                               final String middleName, final String email) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExists(email)) throw new UserEmailExistsException(email);
        final User user = findUserById(id);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Override
    public User updateUserByLogin(final String login, final String lastName, final String firstName,
                                  final String middleName, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyIdException();
        if (isLoginExists(email)) throw new UserLoginExistsException(login);
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = findUserByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

}