package ru.tsc.mordovina.tm.repository;

import ru.tsc.mordovina.tm.api.repository.IUserRepository;
import ru.tsc.mordovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.mordovina.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Override
    public User findUserById(final String id) {
        for (User user : users)
            if (id.equals(user.getId())) return user;
        return null;
    }

    @Override
    public User findUserByLogin(final String login) {
        for (User user : users)
            if (login.equals(user.getLogin())) return user;
        return null;
    }

    @Override
    public User findUserByEmail(final String email) {
        for (User user : users)
            if (email.equals(user.getEmail())) return user;
        return null;
    }

    @Override
    public User removeUserById(final String id) {
        final User user = findUserById(id);
        if (user == null) throw new UserNotFoundException();
        remove(user);
        return user;
    }

    @Override
    public User removeUserByLogin(final String login) {
        final User user = findUserByLogin(login);
        if (user == null) throw new UserNotFoundException();
        remove(user);
        return user;
    }

    @Override
    public boolean userExistsByLogin(String login) {
        return findUserByLogin(login) != null;
    }

    @Override
    public boolean userExistsByEmail(String email) {
        return findUserByEmail(email) != null;
    }

    @Override
    public void remove(final User user) {
        users.remove(user);
    }

    @Override
    public void add(final User user) {
        users.add(user);
    }

}
